<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Empresa extends Model
{
    protected $table = 'empresa';
    protected $primaryKey = 'cuit';
    protected $keyType = 'string';
    public $timestamps = false;
    protected $fillable = array('cuit', 'razon', 'telefono', 'f_ini_acti', 'situacion_revista', 'condicion', 'actividad', 'cod_zona', 'mod_cotrata', 'tipo_empre', 'id_domicilio');
    protected $attributes = [
        'activo' => 1,
    ];
}
