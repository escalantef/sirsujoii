@extends('layouts.app', ['title' => "Mantenimiento de personal"])

@section('content')
    @include('layouts.headers.cards')

    <div class="container-fluid mt--7">
        <div class="row">
            <div class="col">
                <div class="card shadow">
                    <div class="card-header border-0">
                        <div class="row align-items-center">
                            <div class="col-8">
                                <h3 class="mb-0">Mantenimiento de Personal</h3>
                            </div>
                            <div class="col-4 text-right">
                                <a href="{{ route('personal.create') }}" class="btn btn-sm btn-primary">Agregar personal</a>
                            </div>
                        </div>
                    </div>

                    <div class="col-12 text-right my-2">
                      <div class="dropdown">
                        <a class="btn" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                            Filtros
                        </a>
                        <div class="dropdown-menu dropdown-menu-right dropdown-menu-arrow p-3">
                          <div class="row">
                            <div class="col-md-12">
                              <h2>Filtros</h2>
                            </div>
                            <div class="col-md-6">
                              <div class="form-group">
                                <label>Apellido y nombre</label>
                                <input type="text" class="form-control form-control-sm">
                              </div>
                            </div>
                            <div class="col-md-6">
                              <div class="form-group">
                                <label>CUIL</label>
                                <input type="text" class="form-control form-control-sm">
                              </div>
                            </div>
                            <div class="col-md-12 text-right">
                              <button class="btn btn-primary btn-sm" type="button">Filtrar</button>
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>

                    <div class="col-12">
                        @if (session('status'))
                            <div class="alert alert-success alert-dismissible fade show" role="alert">
                                {{ session('status') }}
                                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                </button>
                            </div>
                        @endif
                    </div>

                    <div class="table-responsive">
                        <table class="table align-items-center table-flush">
                            <thead class="thead-light">
                                <tr>
                                    <th scope="col">Legajo</th>
                                    <th scope="col">Apellido y nombre</th>
                                    <th scope="col">C.U.I.L</th>
                                    <th scope="col">Días trabajados</th>
                                    <th scope="col">AFJP</th>
                                    <th scope="col"></th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr>
                                    <td>3</td>
                                    <td>GARGANO RAFAEL ANTONIO</td>
                                    <td>20-1807193-78</td>
                                    <td>30</td>
                                    <td>ORIG</td>
                                    <td class="text-right">
                                        <div class="dropdown">
                                            <a class="btn btn-sm btn-icon-only text-light" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                                <i class="fas fa-ellipsis-v"></i>
                                            </a>
                                            <div class="dropdown-menu dropdown-menu-right dropdown-menu-arrow">
                                                <a class="dropdown-item" href="{{ route('profile.edit') }}">{{ __('Editar') }}</a>
                                            </div>
                                        </div>
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>

        @include('layouts.footers.auth')
    </div>
@endsection


<style>
.dropdown-menu {
    min-width: 60rem !important;
}
</style>