<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Siap_TiposModalidadesContratacion extends Model
{
    protected $table = 'siap_modalidad_contratacion';
    protected $primaryKey = 'codigo';
    public $timestamps = false;
}
