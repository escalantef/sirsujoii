<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDefaultUser extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::table('users')->insert(
            array(
                'email' => 'admin@admin.com',
                'name' => 'admin',
                'password' => '$2y$10$Iq/.Aw/bhNuKQXxMgCZ1Qe5JCA3LCvif/Qeck2.Mcd5rboGb2gszi',
                'created_at' => '2019-06-05 03:36:22',
                'updated_at' => '2019-06-05 03:36:22'
            )
        );
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('default_user');
    }
}
