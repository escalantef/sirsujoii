<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Condicion extends Model
{
    protected $table = 'siap_codigo_condicion';
    protected $primaryKey = 'codigo';
    public $timestamps = false;
}
