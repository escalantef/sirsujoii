<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Empresa;
use App\Models\Domicilio;
use App\Models\SituacionRevista;
use App\Models\Condicion;
use App\Models\Actividad;
use App\Models\Siap_TiposModalidadesContratacion;

class EmpresaController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data['empresas'] = Empresa::all();
        return view('empresas/list', $data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $data['situacionesRevista'] = SituacionRevista::all();
        $data['condiciones'] = Condicion::all();
        $data['actividades'] = Actividad::all();
        $data['modalidadesDeContratacion'] = Siap_TiposModalidadesContratacion::all();
        return view('empresas/create', $data);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $data = $request->all();
        $domicilio = new Domicilio($data);
        $domicilio->save();
        $data['id_domicilio'] = $domicilio->id_domicilio;
        $empresa = new Empresa($data);
        $empresa->save();
        return redirect()->route('empresa.index')->withStatus('Empresa creada correctamente');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
