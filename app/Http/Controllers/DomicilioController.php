<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Pais;
use App\Models\Provincia;
use App\Models\Departamento;
use App\Models\Localidad;

class DomicilioController extends Controller
{
    public function getPaisesAPI()
    {
        return Pais::all();
    }

    public function getProvinciasAPI($pIdPais = 0)
    {
        return Provincia::where('pais', $pIdPais)->get();
    }

    public function getDepartamentosAPI($pIdProvincia)
    {
        return Departamento::where('provincia', $pIdProvincia)->get();
    }

    public function getLocalidadesAPI($pIdDepartamento)
    {
        return Localidad::where('dpto_partido', $pIdDepartamento)->get();
    }
}
