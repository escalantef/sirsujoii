@extends('layouts.app', ['title' => 'Empresas'])

@section('content')
    @include('layouts.headers.crud-header', ['title' => 'Crear empresa'])
    <div class="container-fluid mt--7">
        <div class="row">
            <div class="col-xl-12 order-xl-1">
                <div class="card bg-secondary shadow">
                    <div class="card-header bg-white border-0">
                        <div class="row align-items-center">
                            <div class="col-8">
                                <h3 class="mb-0">Empresas</h3>
                            </div>
                            <div class="col-4 text-right">
                                <a href="{{ route('empresa.index') }}" class="btn btn-sm btn-primary">{{ __('Back to list') }}</a>
                            </div>
                        </div>
                    </div>
                    <div class="card-body">
                        <form method="post" action="{{ route('empresa.store') }}" autocomplete="off">
                            @csrf
                            <h6 class="heading-small text-muted mb-4">Información de la empresa</h6>
                            <div class="row pl-lg-4">
                                @input(['name' => 'cuit', 'label' => 'CUIT','class' => 'col-md-6'])
                                @endinput

                                @input(['name' => 'razon', 'label' => 'Razón Social','class' => 'col-md-6'])
                                @endinput

                                @input(['name' => 'telefono', 'label' => 'Teléfono','class' => 'col-md-4'])
                                @endinput

                                @input(['name' => 'f_ini_acti', 'label' => 'Inicio de actividad','type'=>'date','class' => 'col-md-4'])
                                @endinput

                                @select(['name' => 'situacion_revista', 'label' => 'Situación revista','options'=>$situacionesRevista,'optionValue'=>'codigo','optionText'=> 'descripcion','class' => 'col-md-4'])
                                @endselect

                                @select(['name' => 'condicion', 'label' => 'Condición','options'=>$condiciones,'optionValue'=>'codigo','optionText'=> 'descripcion','class' => 'col-md-4'])
                                @endselect

                                @select(['name' => 'actividad', 'label' => 'Actividad','options'=>$actividades,'optionValue'=>'codigo','optionText'=> 'descripcion','class' => 'col-md-4'])
                                @endselect

                                @input(['name' => 'cod_zona', 'label' => 'Código de zona','type'=>'number','class' => 'col-md-4'])
                                @endinput

                                @select(['name' => 'mod_cotrata', 'label' => 'Modalidad de contratación','options'=>$modalidadesDeContratacion,'optionValue'=>'codigo','optionText'=> 'descripcion','class' => 'col-md-6'])
                                @endselect

                                @php
                                    $tiposEmpresa = [
                                        (Object)['codigo'=> 0,'descripcion' => 'E'],
                                        (Object)['codigo'=> 1,'descripcion' => 'L']
                                    ];
                                @endphp
                                @select(['name' => 'tipo_empre', 'label' => 'Tipo de empresa','options'=>$tiposEmpresa,'optionValue'=>'codigo','optionText'=> 'descripcion','class' => 'col-md-6'])
                                @endselect
                            </div>
                            <h6 class="heading-small text-muted mb-4">Domicilio</h6>
                            <div class="row pl-lg-4">
                                @input(['name' => 'calle', 'label' => 'Calle','class' => 'col-md-3']) @endinput
                                @input(['name' => 'numero', 'label' => 'Número','class' => 'col-md-3']) @endinput
                                @input(['name' => 'piso', 'label' => 'Piso','class' => 'col-md-3']) @endinput
                                @input(['name' => 'dpto', 'label' => 'Dpto','class' => 'col-md-3']) @endinput
                                @input(['name' => 'cod_postal', 'label' => 'Cod. Postal','class' => 'col-md-3']) @endinput
                                <div class="form-group col-md-3">
                                    <label class="form-control-label" >País</label>
                                    <select class="form-control" name="nacion">
                                        <option>Seleccionar</option>
                                    </select>
                                </div>
                                <div class="form-group col-md-3">
                                    <label class="form-control-label" >Provincia</label>
                                    <select class="form-control" name="provincia">
                                        <option>Seleccionar</option>
                                    </select>
                                </div>
                                <div class="form-group col-md-3">
                                    <label class="form-control-label" >Departamento</label>
                                    <select class="form-control" name="dep_partid">
                                        <option>Seleccionar</option>
                                    </select>
                                </div>
                                <div class="form-group col-md-3">
                                    <label class="form-control-label" >Localidad</label>
                                    <select class="form-control" name="localidad">
                                        <option>Seleccionar</option>
                                    </select>
                                </div>
                                <div class="text-center col-md-12">
                                    <button type="submit" class="btn btn-success mt-4">{{ __('Save') }}</button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
        @include('layouts.footers.auth')
    </div>
@endsection

@push('js')
    <script>
        var selectNacion = $("select[name='nacion']");
        var selectProvincia = $("select[name='provincia']");
        var selectDepartamento = $("select[name='dep_partid']");
        var selectLocalidad = $("select[name='localidad']");

        $(function() {
            getPaises();
            selectNacion.change(function() {
                getProvincias(selectNacion.val());
            });
            selectProvincia.change(function() {
                getDepartamentos(selectProvincia.val());
            });
            selectDepartamento.change(function() {
                getLocalidades(selectDepartamento.val());
            });
        });

        function getPaises() {
            selectNacion.children('option:not(:first)').remove();
            $.get("{{ url('/api/domicilio/getPaises') }}", function( data ) {
                $.each(data ,function(key, value)
                {
                    selectNacion.append('<option value=' + value.pais + '>' + value.nombre + '</option>');
                });
            });
        }

        function getProvincias(pIdPais) {
            selectProvincia.children('option:not(:first)').remove();
            $.get("{{ url('/api/domicilio/getProvincias') }}" + '/' + pIdPais, function( data ) {
                $.each(data ,function(key, value)
                {
                    selectProvincia.append('<option value=' + value.provincia + '>' + value.nombre + '</option>');
                });
            });
        }

        function getDepartamentos(pIdProvincia) {
            selectDepartamento.children('option:not(:first)').remove();
            $.get("{{ url('/api/domicilio/getDepartamentos') }}" + '/' + pIdProvincia, function( data ) {
                $.each(data ,function(key, value)
                {
                    selectDepartamento.append('<option value=' + value.dpto_partido + '>' + value.nombre + '</option>');
                });
            });
        }

        function getLocalidades(pIdDepartamento) {
            selectLocalidad.children('option:not(:first)').remove();
            $.get("{{ url('/api/domicilio/getLocalidades') }}" + '/' + pIdDepartamento, function( data ) {
                $.each(data ,function(key, value)
                {
                    selectLocalidad.append('<option value=' + value.localidad + '>' + value.nombre + '</option>');
                });
            });
        }
    </script>
@endpush