<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Domicilio extends Model
{
    protected $table = 'domicilios';
    protected $primaryKey = 'id_domicilio';
    public $timestamps = false;
    protected $fillable = array('calle', 'numero', 'piso', 'dpto', 'cod_postal', 'localidad', 'dep_partid', 'provincia', 'nacion');
    protected $attributes = [
        'activo' => 1,
    ];
}
