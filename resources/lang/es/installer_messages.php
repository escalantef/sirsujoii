<?php

return [

    /**
     *
     * Traducciones compartidas.
     *
     */
    'title' => 'Instalador de Laravel',
    'next' => 'Siguiente',
    'finish' => 'Instalar',


    /**
     *
     * Traducciones de la página principal.
     *
     */
    'welcome' => [
        'title'   => 'Bienvenido al instalador',
        'message' => 'Bienvenido al asistente de configuración',
        'next' => 'Siguiente',
    ],


    /**
     *
     * Tranducciones de la página de requisitos.
     *
     */
    'requirements' => [
        'title' => 'Requisitos',
        'next' => 'Siguiente',
    ],


    /**
     *
     * Traducciones de la pagina de permisos.
     *
     */
    'permissions' => [
        'title' => 'Permisos',
        'next' => 'Siguiente',
    ],


    /**
     *
     * Traducciones de la página de entorno.
     *
     */
    'environment' => [
        'menu' => [
            'templateTitle' => 'Paso 3 | Configuración de entorno',
            'title' => 'Configuración de entorno',
            'desc' => 'Por favor seleccione como si quisiera modificar el .env',
            'wizard-button' => 'Instalador mediante formulario',
            'classic-button' => 'Editor clásico de texto',
        ],
        'wizard' => [
            'templateTitle' => 'Paso 3 | Configuración de entorno',
            'title' => 'Instalación',
            'tabs' => [
                'environment' => 'Entorno',
                'database' => 'Base de datos',
                'application' => 'Aplicación'
            ],
            'form' => [
                'name_required' => 'Un nombre de entorno es requerido.',
                'app_name_label' => 'Nombre de aplicación',
                'app_name_placeholder' => 'Nombre de la aplicación',
                'app_environment_label' => 'Entono de la aplicación',
                'app_environment_label_local' => 'Local',
                'app_environment_label_developement' => 'Desarrollo',
                'app_environment_label_qa' => 'Pruebas',
                'app_environment_label_production' => 'Producción',
                'app_environment_label_other' => 'Otro',
                'app_environment_placeholder_other' => 'Ingrese su entorno...',
                'app_debug_label' => 'App Debug',
                'app_debug_label_true' => 'Si',
                'app_debug_label_false' => 'No',
                'app_log_level_label' => 'Nivel de log',
                'app_log_level_label_debug' => 'debug',
                'app_log_level_label_info' => 'info',
                'app_log_level_label_notice' => 'notice',
                'app_log_level_label_warning' => 'warning',
                'app_log_level_label_error' => 'error',
                'app_log_level_label_critical' => 'critical',
                'app_log_level_label_alert' => 'alert',
                'app_log_level_label_emergency' => 'emergency',
                'app_url_label' => 'URL de la aplicación',
                'app_url_placeholder' => 'URL de la aplicación',
                'db_connection_label' => 'Conexión de la base de datos',
                'db_connection_label_mysql' => 'mysql',
                'db_connection_label_sqlite' => 'sqlite',
                'db_connection_label_pgsql' => 'pgsql',
                'db_connection_label_sqlsrv' => 'sqlsrv',
                'db_host_label' => 'Database Host',
                'db_host_placeholder' => 'Database Host',
                'db_port_label' => 'Database Port',
                'db_port_placeholder' => 'Database Port',
                'db_name_label' => 'Database Name',
                'db_name_placeholder' => 'Database Name',
                'db_username_label' => 'Database User Name',
                'db_username_placeholder' => 'Database User Name',
                'db_password_label' => 'Database Password',
                'db_password_placeholder' => 'Database Password',

                'app_tabs' => [
                    'more_info' => 'Más info',
                    'broadcasting_title' => 'Broadcasting, Caching, Session, &amp; Queue',
                    'broadcasting_label' => 'Broadcast Driver',
                    'broadcasting_placeholder' => 'Broadcast Driver',
                    'cache_label' => 'Cache Driver',
                    'cache_placeholder' => 'Cache Driver',
                    'session_label' => 'Session Driver',
                    'session_placeholder' => 'Session Driver',
                    'queue_label' => 'Queue Driver',
                    'queue_placeholder' => 'Queue Driver',
                    'redis_label' => 'Redis Driver',
                    'redis_host' => 'Redis Host',
                    'redis_password' => 'Redis Password',
                    'redis_port' => 'Redis Port',

                    'mail_label' => 'Mail',
                    'mail_driver_label' => 'Mail Driver',
                    'mail_driver_placeholder' => 'Mail Driver',
                    'mail_host_label' => 'Mail Host',
                    'mail_host_placeholder' => 'Mail Host',
                    'mail_port_label' => 'Mail Port',
                    'mail_port_placeholder' => 'Mail Port',
                    'mail_username_label' => 'Mail Username',
                    'mail_username_placeholder' => 'Mail Username',
                    'mail_password_label' => 'Mail Password',
                    'mail_password_placeholder' => 'Mail Password',
                    'mail_encryption_label' => 'Mail Encryption',
                    'mail_encryption_placeholder' => 'Mail Encryption',

                    'pusher_label' => 'Pusher',
                    'pusher_app_id_label' => 'Pusher App Id',
                    'pusher_app_id_palceholder' => 'Pusher App Id',
                    'pusher_app_key_label' => 'Pusher App Key',
                    'pusher_app_key_palceholder' => 'Pusher App Key',
                    'pusher_app_secret_label' => 'Pusher App Secret',
                    'pusher_app_secret_palceholder' => 'Pusher App Secret',
                ],
                'buttons' => [
                    'setup_database' => 'Configurar la base de datos',
                    'setup_application' => 'Configurar aplicación',
                    'install' => 'Instalar',
                ],
            ],
        ],
        'classic' => [
            'templateTitle' => 'Step 3 | Configuración del entorno | Editor clásico',
            'title' => 'Editor clásico de entorno',
            'save' => 'Guardar .env',
            'back' => 'Use instalador básico',
            'install' => 'Guardar e instalar',
        ],
        'success' => 'Tu archivo de entorno (.env) ha sido guardado.',
        'errors' => 'Imposible guardar el archivo de configuración. Por favor creelo manualmente.',
    ],


    /**
     *
     * Traducciones de la página final.
     *
     */
    'final' => [
        'title' => 'Finalizado.',
        'finished' => 'La aplicación ha sido instalada con éxito!',
        'exit' => 'Haz click aquí para salir.',
        'next' => 'Siguiente',
    ],
];
