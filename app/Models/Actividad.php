<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Actividad extends Model
{
    protected $table = 'siap_actividad';
    protected $primaryKey = 'codigo';
    public $timestamps = false;
}
