@extends('layouts.app', ['title' => "Empresas"])

@section('content')
    @include('layouts.headers.cards')

    <div class="container-fluid mt--7">
        <div class="row">
            <div class="col">
                <div class="card shadow">
                    <div class="card-header border-0">
                        <div class="row align-items-center">
                            <div class="col-8">
                                <h3 class="mb-0">Empresas</h3>
                            </div>
                            <div class="col-4 text-right">
                                <a href="{{ route('empresa.create') }}" class="btn btn-sm btn-primary">Agregar empresa</a>
                            </div>
                        </div>
                    </div>

                    <div class="col-12">
                        @if (session('status'))
                            <div class="alert alert-success alert-dismissible fade show" role="alert">
                                {{ session('status') }}
                                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                </button>
                            </div>
                        @endif
                    </div>

                    <div class="table-responsive">
                        <table class="table align-items-center table-flush">
                            <thead class="thead-light">
                                <tr>
                                    <th scope="col">Tipo</th>
                                    <th scope="col">Razón social</th>
                                    <th scope="col">C.U.I.T</th>
                                    <th scope="col"></th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach ($empresas as $empresa)
                                <tr>
                                    <td> {{ $empresa->tipo_empre }} </td>
                                    <td> {{ $empresa->razon }} </td>
                                    <td> {{ $empresa->cuit }} </td>
                                    <td class="text-right">
                                        @php $opciones = [ ['ruta' => route('empresa.edit', $empresa),'texto'=> 'Editar'] ] @endphp
                                        @botonEditar(['opciones'=>$opciones])
                                        @endbotonEditar
                                    </td>
                                </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>

        @include('layouts.footers.auth')
    </div>
@endsection
