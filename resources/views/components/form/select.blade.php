@php
    $selected = $selected ?? 0;
@endphp
<div class="form-group {{ $class ?? 'col-md-12' }}">
    <label class="form-control-label" >{{ $label }}</label>
    <select class="form-control" name="{{ $name }}">
      @foreach ($options as $option)
      <option value="{{ $option->$optionValue }}" {{ $selected == $option->$optionValue ? 'selected' : ''  }}> {{ $option->$optionText }} </option>
      @endforeach
    </select>
</div>