<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class SituacionRevista extends Model
{
    protected $table = 'siap_situacion_revista';
    protected $primaryKey = 'codigo';
    public $timestamps = false;
}
