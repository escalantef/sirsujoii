<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::get('domicilio/getPaises', 'DomicilioController@getPaisesAPI');
Route::get('domicilio/getProvincias/{pIdPais}', 'DomicilioController@getProvinciasAPI');
Route::get('domicilio/getDepartamentos/{pIdProvincia}', 'DomicilioController@getDepartamentosAPI');
Route::get('domicilio/getLocalidades/{pIdDepartamento}', 'DomicilioController@getLocalidadesAPI');



Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});
