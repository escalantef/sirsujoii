<div class="form-group {{ $class ?? 'col-md-12' }}">
  <label class="form-control-label" >{{ $label }}</label>
  <input type="{{ $type ?? 'text' }}" name="{{ $name }}" class="form-control form-control-alternative" {{ $required ?? '' }} autofocus>
</div>