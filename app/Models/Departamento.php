<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Departamento extends Model
{
    protected $table = 'dptos_partidos';
    protected $primaryKey = 'dpto_partido';
    public $timestamps = false;
}
